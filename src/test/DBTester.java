package test;

import static org.junit.jupiter.api.Assertions.*;

import java.io.File;

import org.junit.jupiter.api.Test;

import hw5.DB;
import hw5.DBCollection;

class DBTester {
	
	/**
	 * Things to consider testing:
	 * 
	 * Properly creates directory for new DB (done)
	 * Properly accesses existing directory for existing DB
	 * Properly accesses collection
	 * Properly drops a database
	 * Special character handling?
	 */
	
	
	@Test
	public void testCreateDB() {
		DB hw5 = new DB("hw5"); //call method
		assertTrue(new File("testfiles/hw5").exists()); //verify results
	}
	
	@Test
	public void testDBAccess() {
		//Properly accesses existing directory for existing DB
		DB data = new DB("data");
		DBCollection test = data.getCollection("test");
		
		DB db2 = new DB("data2");
		DBCollection test2 = data.getCollection("test2");
		
		assertTrue(new File("testfiles/data").exists());
		assertTrue(new File("testfiles/data2").exists());
	}
	
	
	@Test
	 public void testCreateMultipleDB() {
		DB data = new DB("data");
		DB data2 = new DB("data2");
		DB data3 = new DB("data3");
		
		assertTrue(new File("testfiles/data").exists());
		new File("testfiles/data").delete();
		
		assertTrue(new File("testfiles/data2").exists());
		new File("testfiles/data2").delete();
		
		assertTrue(new File("testfiles/data3").exists());
		new File("testfiles/data3").delete();
	}
	
	
	@Test
	public void testAccessCollection() {
		//test the DB called data contains the collection named test
		DB data = new DB("data");
		DBCollection test = data.getCollection("test");
		assertTrue(data.DBhasCollections("test"));
		assertTrue(!data.DBhasCollections("apple")); 

		
		//test get collection
		// a new one
		String collectName1 = "aaaa";
		if(data.DBhasCollections(collectName1)) {
			fail("should not contain the collection");
		}
		
		data.getCollection(collectName1); // create a new collection;
		if(!data.DBhasCollections(collectName1)){
			fail("should contain the collectio");
		}
		
		//test exist collection
		String collectName2 = "test";
		if(!data.getCollection(collectName2).getDocument(0).getAsJsonPrimitive("key").getAsString().equals("value")) {
			fail("fail to access the collection called test");
		}
		
		if(!data.getCollection(collectName2).getDocument(1).getAsJsonObject("embedded").getAsJsonPrimitive("key2").getAsString().equals("value2")) {
			fail("fail to access the document");
		}
		

		//delete collection
		data.deleteCollection(collectName1);
		assertTrue(!data.DBhasCollections(collectName1));
	}
	

	
	@Test
	public void testDropDB() {
		//Properly drops a database 
		//empty DB
		DB hw5 = new DB("hw5");
		assertTrue(new File("testfiles/hw5").exists());
		hw5.dropDatabase();
		assertFalse(new File("testfiles/hw5").exists());
		
		//not empty DB
		DB hw5_2 = new DB("hw5_2");
		//create these collection
		hw5_2.getCollection("key1");
		hw5_2.getCollection("key2");
		hw5_2.getCollection("key3");
		
		assertTrue(hw5_2.DBhasCollections("key1"));
		assertTrue(hw5_2.DBhasCollections("key2"));
		assertTrue(hw5_2.DBhasCollections("key3"));
		
		hw5_2.dropDatabase();
		DB hw5_1 = new DB("sample2");
		
		// whether these 3 collections exist
		if(hw5_1.DBhasCollections("key1") || hw5_1.DBhasCollections("key2") || hw5_1.DBhasCollections("key3")) {
			fail("fail to drop the database!");
		}
	}
	
	
	//Special character(upper case and lower case) handling
	@Test
	public void testSpecialCharacter() {
		DB data = new DB("data");
		DBCollection test = data.getCollection("TEST");
		// Upper case chars and Lower case char are considered
		// as different. 
		assertTrue(!data.DBhasCollections("test"));
		assertTrue(data.DBhasCollections("TEST"));
	}
}
