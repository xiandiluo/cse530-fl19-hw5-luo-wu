package test;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.google.gson.JsonObject;

import hw5.DB;
import hw5.DBCollection;
import hw5.DBCursor;
import hw5.Document;

class CursorTester {
	/**
	 * Things to consider testing:
	 * 
	 * hasNext (done?) count (done?) next (done?)
	 * 
	 * Queries: Find all Find with relational select Conditional operators Embedded
	 * documents Arrays Find with relational project
	 */

	@BeforeAll
	public static void initial() {
		DB db = new DB("query");
		DBCollection test1 = db.getCollection("test1");
		String json1 = "{ item: \"bookA\", character: { price: 30, num: 3 }, status : \"A\" }";
		JsonObject one = Document.parse(json1);
		String json2 = "{ item: \"bookB\", character: { price: 20, num: 2 }, status : \"A\" }";
		JsonObject two = Document.parse(json2);
		String json3 = "{ item: \"bookC\", character: { price: 10, num: 1 }, status : \"B\" }";
		JsonObject three = Document.parse(json3);
		String json4 = "{ item: \"bookD\", character: { price: 10, num: 1 }, status : \"C\" }";
		JsonObject four = Document.parse(json4);
		test1.insert(one, two, three, four);

		DBCollection test2 = db.getCollection("test2");
		String json5 = "{ item: \"bookA\", character:[ \"red\", \"yellow\" ] }";
		JsonObject five = Document.parse(json5);
		String json6 = "{ item: \"bookB\", character: [ \"red\", \"black\" ] }";
		JsonObject six = Document.parse(json6);
		String json7 = "{ item: \"bookC\", character: [ \"red\", \"black\", \"plain\" ] }";
		JsonObject seven = Document.parse(json7);
		String json8 = "{ item: \"bookD\", character: [ \"blue\" ] }";
		JsonObject eight = Document.parse(json8);
		test2.insert(five, six, seven, eight);
	}
	
	/*
	 * Test for collection as following:
	 * 
	 * test1:
	 * [
   			{ item: "bookA", character: { price: 30, num: 3 }, status : "A"},
   			{ item: "bookB", character: { price: 20, num: 2 }, status : "B"},
   			{ item: "bookC", character: { price: 10, num: 1 }, status : "C"},
   			{ item: "bookD", character: { price: 10, num: 1 }, status : "D"},
		]
	 *
		test2:
		[
			{ item: "bookA", character: { red, yellow }, },
   			{ item: "bookB", character: { red, black } },
   			{ item: "bookC", character: { red, black, plain } },
   			{ item: "bookD", character: { blue } },
   		]
	 * */
	
	@Test
	public void testFindAll() {
		DB db = new DB("data");
		DBCollection test = db.getCollection("test");
		DBCursor results = test.find();

		assertTrue(results.count() == 3);
		assertTrue(results.hasNext());
		JsonObject d1 = results.next(); // pull first document
		// verify contents?
		assertTrue(results.hasNext());// still more documents
		JsonObject d2 = results.next(); // pull second document
		// verfiy contents?
		assertTrue(results.hasNext()); // still one more document
		JsonObject d3 = results.next();// pull last document
		assertFalse(results.hasNext());// no more documents
	}
	
	@Test
	public void testFindAllWithVerifyContent() {
		DB db = new DB("data");
		DBCollection test = db.getCollection("test");
		DBCursor results = test.find();

		assertTrue(results.count() == 3);
		assertTrue(results.hasNext());
		JsonObject d1 = results.next(); // pull first document

		// verify contents
		assertTrue(d1.getAsJsonPrimitive("key").getAsString().equals("value"));
		assertTrue(results.hasNext());// still more documents
		JsonObject d2 = results.next(); // pull second document

		// verfiy contents
		assertTrue(d2.getAsJsonObject("embedded").getAsJsonPrimitive("key2").getAsString().equals("value2"));
		assertTrue(results.hasNext()); // still one more document
		JsonObject d3 = results.next();// pull last document

		// verfiy contents
		assertTrue(d3.getAsJsonArray("array").get(0).getAsString().equals("one"));
		assertTrue(d3.getAsJsonArray("array").get(1).getAsString().equals("two"));
		assertTrue(d3.getAsJsonArray("array").get(2).getAsString().equals("three"));
		assertFalse(results.hasNext());// no more documents
	}
	
	@Test
	public void testOwnCollection() {
		DB db = new DB("query");
		DBCollection test = db.getCollection("test1");
		DBCursor results = test.find();
		assertTrue(results.count() == 4);
		assertTrue(results.hasNext());
		while (results.hasNext()) {
			JsonObject d1 = results.next(); 
			System.out.println("Collection is: " + d1.toString());
		}
	}

	@Test
	public void testRelationalSelectEmbeddedDocument() {
		DB db = new DB("query");
		DBCollection test1 = db.getCollection("test1");

		// Match an Embedded/Nested Document
		DBCursor cursor = test1.find(Document.parse("{item : bookA }"));
		assertTrue(cursor.count() == 1);
		assertTrue(cursor.hasNext());
		JsonObject obj1 = cursor.next();
//		System.out.println(obj1.toString());
		
		assertTrue(obj1.getAsJsonPrimitive("item").getAsString().equals("bookA"));
		assertTrue(obj1.getAsJsonObject("character").getAsJsonPrimitive("price").getAsString().equals("30"));
		assertTrue(obj1.getAsJsonObject("character").getAsJsonPrimitive("num").getAsString().equals("3"));
		assertFalse(cursor.hasNext());
	}

	@Test
	public void testRelationalSelectNestedField() {
		// Query on Nested Field
		DB db = new DB("query");
		DBCollection test1 = db.getCollection("test1");
		DBCursor cursor2 = test1.find(Document.parse("{\"character.price\" : 10}"));
		// DBCursor results = test1.find(Document.parse("{item : bookA }"));
		assertTrue(cursor2.count() == 2);
		assertTrue(cursor2.hasNext());
		JsonObject obj2 = cursor2.next();
//		System.out.println(obj2.toString());	// print for test
		
		assertTrue(obj2.getAsJsonPrimitive("item").getAsString().equals("bookC"));
		assertTrue(obj2.getAsJsonObject("character").getAsJsonPrimitive("price").getAsString().equals("10"));
		assertTrue(obj2.getAsJsonObject("character").getAsJsonPrimitive("num").getAsString().equals("1"));
		assertTrue(cursor2.hasNext());
		JsonObject obj3 = cursor2.next();
		assertTrue(obj3.getAsJsonPrimitive("item").getAsString().equals("bookD"));
		assertTrue(obj3.getAsJsonObject("character").getAsJsonPrimitive("price").getAsString().equals("10"));
		assertTrue(obj3.getAsJsonObject("character").getAsJsonPrimitive("num").getAsString().equals("1"));
		assertFalse(cursor2.hasNext());
	}

	@Test
	public void testSelectComaprsionEQandNE() {
		DB database = new DB("query");
		DBCollection test = database.getCollection("test1");

		String query = "{ \"character.price\": { $eq: 20 } }";
		DBCursor cursor = test.find(Document.parse(query));
		assertTrue(cursor.count() == 1);
		assertTrue(cursor.hasNext());
		JsonObject obj1 = cursor.next();
		assertTrue(obj1.getAsJsonPrimitive("item").getAsString().equals("bookB"));
		assertTrue(obj1.getAsJsonObject("character").getAsJsonPrimitive("price").getAsString().equals("20"));
		assertFalse(cursor.hasNext());

		String query2 = "{ \"character.num\": { $eq: 3 } }";
		DBCursor cursor2 = test.find(Document.parse(query2));
		assertTrue(cursor2.count() == 1);
		assertTrue(cursor2.hasNext());

		JsonObject obj2 = cursor2.next();
		assertTrue(obj2.getAsJsonPrimitive("item").getAsString().equals("bookA"));
		assertTrue(obj2.getAsJsonObject("character").getAsJsonPrimitive("num").getAsString().equals("3"));
		assertFalse(cursor2.hasNext());

		// NE: not equal
		String query3 = "{ \"character.price\": { $ne: 10 } }";
		DBCursor cursor3 = test.find(Document.parse(query3));
		assertTrue(cursor3.count() == 2);
		assertTrue(cursor3.hasNext());
		JsonObject obj3 = cursor3.next();
		assertTrue(obj3.getAsJsonPrimitive("item").getAsString().equals("bookA"));
		assertTrue(obj3.getAsJsonObject("character").getAsJsonPrimitive("price").getAsString().equals("30"));
		assertTrue(cursor3.hasNext());
		obj3 = cursor3.next();
		assertTrue(obj3.getAsJsonPrimitive("item").getAsString().equals("bookB"));
		assertTrue(obj1.getAsJsonObject("character").getAsJsonPrimitive("price").getAsString().equals("20"));
		assertFalse(cursor3.hasNext());

	}

	@Test
	public void testSelectComaprsionLT() {
		DB database = new DB("query");
		DBCollection test = database.getCollection("test1");

		// less than
		String query = "{ \"character.price\": { $lt: 15 } }";
		DBCursor cursor = test.find(Document.parse(query));
		assertTrue(cursor.count() == 2);
		assertTrue(cursor.hasNext());
		JsonObject obj1 = cursor.next();
		assertTrue(obj1.getAsJsonPrimitive("item").getAsString().equals("bookC"));
		assertTrue(obj1.getAsJsonObject("character").getAsJsonPrimitive("price").getAsNumber().intValue() < 15);
		assertTrue(cursor.hasNext());
		obj1 = cursor.next();
		assertTrue(obj1.getAsJsonPrimitive("item").getAsString().equals("bookD"));
		assertTrue(obj1.getAsJsonObject("character").getAsJsonPrimitive("price").getAsNumber().intValue() < 15);
		assertFalse(cursor.hasNext());

		// less than and equal
		String query2 = "{ \"character.num\": { $lte: 1 } }";
		DBCursor cursor2 = test.find(Document.parse(query2));
		assertTrue(cursor2.count() == 2);
		assertTrue(cursor2.hasNext());
		JsonObject obj2 = cursor2.next();
		assertTrue(obj2.getAsJsonPrimitive("item").getAsString().equals("bookC"));
		assertTrue(obj2.getAsJsonObject("character").getAsJsonPrimitive("num").getAsNumber().intValue() <= 1);
		obj2 = cursor2.next();
		assertTrue(obj2.getAsJsonPrimitive("item").getAsString().equals("bookD"));
		assertTrue(obj2.getAsJsonObject("character").getAsJsonPrimitive("num").getAsNumber().intValue() <= 1);
		assertFalse(cursor2.hasNext());
	}

	@Test
	public void testSelectComaprsionGT() {
		DB database = new DB("query");
		DBCollection test = database.getCollection("test1");

		// great than
		String query = "{ \"character.price\": { $gt: 15 } }";
		DBCursor cursor = test.find(Document.parse(query));
		assertTrue(cursor.count() == 2);
		assertTrue(cursor.hasNext());
		JsonObject obj1 = cursor.next();
		assertTrue(obj1.getAsJsonPrimitive("item").getAsString().equals("bookA"));
		assertTrue(obj1.getAsJsonObject("character").getAsJsonPrimitive("price").getAsNumber().intValue() > 15);
		assertTrue(cursor.hasNext());
		obj1 = cursor.next();
		assertTrue(obj1.getAsJsonPrimitive("item").getAsString().equals("bookB"));
		assertTrue(obj1.getAsJsonObject("character").getAsJsonPrimitive("price").getAsNumber().intValue() > 15);
		assertFalse(cursor.hasNext());

		// great than and equal
		String query2 = "{ \"character.num\": { $gte: 2 } }";
		DBCursor cursor2 = test.find(Document.parse(query2));
		assertTrue(cursor2.count() == 2);
		assertTrue(cursor2.hasNext());
		JsonObject obj2 = cursor2.next();
		assertTrue(obj2.getAsJsonPrimitive("item").getAsString().equals("bookA"));
		assertTrue(obj2.getAsJsonObject("character").getAsJsonPrimitive("num").getAsNumber().intValue() >= 2);
		obj2 = cursor2.next();
		assertTrue(obj2.getAsJsonPrimitive("item").getAsString().equals("bookB"));
		assertTrue(obj2.getAsJsonObject("character").getAsJsonPrimitive("num").getAsNumber().intValue() >= 2);
		assertFalse(cursor2.hasNext());
	}

	@Test
	public void testFindWithProjection() {
		DB db = new DB("query");
		DBCollection collection = new DBCollection(db, "test1");

		String query = "{ status: \"A\" }";
		JsonObject queryObj = Document.parse(query);
		String projection = "{ \"item\": 0, \"status\": 0 }";
		JsonObject projectObj = Document.parse(projection);

		DBCursor cursor = collection.find(queryObj, projectObj);
		//System.out.println(cursor.count());
		assertTrue(cursor.count() == 2);
		assertTrue(cursor.hasNext());

		JsonObject obj1 = cursor.next();
		//System.out.println(obj1.toString());
		assertTrue(cursor.hasNext());
		while (cursor.hasNext()) {
			assertTrue(!cursor.next().has("status"));
		}
	}

	@Test
	public void testFindWithProjection1() {
		DB db = new DB("query");
		DBCollection test = db.getCollection("test2");
		String project = "{ \"item\": \"1\"}";
		DBCursor results = test.find(Document.parse("{}"), Document.parse(project));
//		System.out.println(results.count());
		assertTrue(results.count() == 4);
		while (results.hasNext()) {
			assertTrue(results.next().has("item"));
		}
	}

	@Test
	public void testFindWithProjection2() {
		DB db = new DB("query");
		DBCollection test = db.getCollection("test2");
		String project = "{\"item\": 0}";
		DBCursor results = test.find(Document.parse("{}"), Document.parse(project));
		
		assertTrue(results.count() == 4);
		while (results.hasNext()) {
			assertTrue(!results.next().has("item"));
		}
	}

	@AfterAll
	public static void clean() {
		DB db = new DB("query");
		db.dropDatabase();
	}
}
