package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.google.gson.JsonObject;

import hw5.Document;

class DocumentTester {
	
	/*
	 * Things to consider testing:
	 * 
	 * Invalid JSON
	 * 
	 * Properly parses embedded documents
	 * Properly parses arrays
	 * Properly parses primitives (done!)
	 * 
	 * Object to embedded document
	 * Object to array
	 * Object to primitive
	 */
	
	@Test
	public void testParsePrimitive() {
		String json = "{ \"key\":\"value\" }";//setup
		JsonObject results = Document.parse(json); //call method to be tested
		assertTrue(results.getAsJsonPrimitive("key").getAsString().equals("value")); //verify results
		
	}
	
	@Test
	public void testParseEmbedded() {
		String json = "{ \"embedded\": { \"key2\": \"value2\" } }";//setup
		JsonObject results = Document.parse(json); //call method to be tested
		assertTrue(results.getAsJsonObject("embedded").getAsJsonPrimitive("key2").getAsString().equals("value2")); //verify results
		
	}
	
	@Test
	public void testParseArrays() {
		String json = "{ \"array\" : [\"one\", \"two\", \"three\"] }";//setup
		JsonObject results = Document.parse(json); //call method to be tested
		if(results.getAsJsonArray("array").size() != 3) {
			fail("the size of parsing array should be 3");
		}

		assertTrue(results.getAsJsonArray("array").get(0).getAsString().equals("one"));
		assertTrue(results.getAsJsonArray("array").get(1).getAsString().equals("two"));
		assertTrue(results.getAsJsonArray("array").get(2).getAsString().equals("three"));
		
	}
	

}
