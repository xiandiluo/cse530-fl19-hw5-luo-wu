package test;

import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.File;

import org.junit.jupiter.api.Test;

import com.google.gson.JsonObject;

import hw5.DB;
import hw5.DBCollection;
import hw5.Document;

class CollectionTester {
	
	/**
	 * Things to consider testing
	 * 
	 * Queries:
	 * 	Find all
	 * 	Find with relational select
	 * 		Conditional operators
	 * 		Embedded documents
	 * 		Arrays
	 * 	Find with relational project
	 * 
	 * Inserts
	 * Updates
	 * Deletes
	 * 
	 * getDocument (done?)
	 * drop
	 */
	
	
	
	@Test
	public void testGetDocument() {
		DB db = new DB("data");
		DBCollection test = db.getCollection("test");
		JsonObject primitive = test.getDocument(0);
		assertTrue(primitive.getAsJsonPrimitive("key").getAsString().equals("value"));
		
	}

	
	@Test
	public void testInserts() {
		DB inserts = new DB("inserts");
		DBCollection test = inserts.getCollection("test");
		String json1 = "{ \"itemA\":\"bookA\" }";
		JsonObject one = Document.parse(json1); 
		String json2 = "{ \"embedded\": { \"itemB\": \"bookB\" } }";
		JsonObject two = Document.parse(json2); 
		String json3 = "{ \"array\" : [\"price\", \"num\", \"address\"] }";
		JsonObject three = Document.parse(json3); 
		
		test.insert(one, two, three);
		assertTrue(test.getDocument(0).getAsJsonPrimitive("itemA").getAsString().equals("bookA"));
		assertTrue(test.getDocument(1).getAsJsonObject("embedded").getAsJsonPrimitive("itemB").getAsString().equals("bookB"));
		if(test.getDocument(2).getAsJsonArray("array").size() != 3) {
			fail("the size of parsing array should be 3");
		}
		assertTrue(test.getDocument(2).getAsJsonArray("array").get(0).getAsString().equals("price"));
		assertTrue(test.getDocument(2).getAsJsonArray("array").get(1).getAsString().equals("num"));
		assertTrue(test.getDocument(2).getAsJsonArray("array").get(2).getAsString().equals("address"));
		inserts.dropDatabase();
	}
	
	@Test
	public void testUpdates() {
		DB update = new DB("update");
		DBCollection test = update.getCollection("test2");
		String json1 = "{ item: \"bookA\", price: \"30\"}";
		test.insert(Document.parse(json1));
		assertTrue(test.getDocument(0).getAsJsonPrimitive("item").getAsString().equals("bookA"));
		assertTrue(test.getDocument(0).getAsJsonPrimitive("price").getAsString().equals("30"));
		
		JsonObject query = Document.parse("{ item: \"bookA\"}");
		JsonObject updates = Document.parse("{ item: \"bookE\", price: 20 }");
		test.update(query, updates, false);
		assertTrue(test.getDocument(0).getAsJsonPrimitive("item").getAsString().equals("bookE"));
		assertTrue(test.getDocument(0).getAsJsonPrimitive("price").getAsString().equals("20"));
		update.dropDatabase();
	}
	
	
	@Test
	public void testUpdatesMulti() {
		DB update = new DB("update");
		DBCollection test = update.getCollection("test5");
		String json1 = "{ item: \"bookA\", price: 30 }";
		String json2 = "{ item: \"bookB\", price: 20 }";
		String json3 = "{ item: \"bookC\", price: 20 }";
		JsonObject one = Document.parse(json1);
		JsonObject two = Document.parse(json2);
		JsonObject three = Document.parse(json3);
		test.insert(one, two, three);
		assertTrue(test.getDocument(0).getAsJsonPrimitive("item").getAsString().equals("bookA"));
		assertTrue(test.getDocument(0).getAsJsonPrimitive("price").getAsString().equals("30"));
		
		JsonObject query = Document.parse("{ price: 20 }");
		JsonObject updates = Document.parse("{ item: \"bookE\", price: 10 }");
		test.update(query, updates, true);
		assertTrue(test.getDocument(1).getAsJsonPrimitive("item").getAsString().equals("bookE"));
		assertTrue(test.getDocument(1).getAsJsonPrimitive("price").getAsString().equals("10"));
		assertTrue(test.getDocument(2).getAsJsonPrimitive("item").getAsString().equals("bookE"));
		assertTrue(test.getDocument(2).getAsJsonPrimitive("price").getAsString().equals("10"));
		update.dropDatabase();
	}
	
	
	@Test
	public void testDeletes() {
		DB delete = new DB("delete");
		DBCollection test = delete.getCollection("remove2");
		String json1 = "{ item: \"bookA\", price: 30 }";
		JsonObject one = Document.parse(json1); 
		String json2 = "{ item: \"bookB\", price: 20 }";
		JsonObject two = Document.parse(json2); 
		String json3 = "{ item: \"bookC\", price: 20 }";
		JsonObject three = Document.parse(json3); 
		String json4 = "{ item: \"bookD\", price: 30 }";
		JsonObject four = Document.parse(json4);
		
		test.insert(one, two, three, four);
		JsonObject query = Document.parse("{ price: 20 }");
		
		//test mlti = true
		test.remove(query, true);
		assertTrue(test.count() == 2);
		assertTrue(test.getDocument(0).getAsJsonPrimitive("item").getAsString().equals("bookA"));
		assertTrue(test.getDocument(0).getAsJsonPrimitive("price").getAsString().equals("30"));
		assertTrue(test.getDocument(1).getAsJsonPrimitive("item").getAsString().equals("bookD"));
		assertTrue(test.getDocument(1).getAsJsonPrimitive("price").getAsString().equals("30"));
		
		//test mlti = false
		JsonObject query2 = Document.parse("{ price: 30 }");
		test.remove(query2, false);
		assertTrue(test.count() == 1);
		assertTrue(test.getDocument(0).getAsJsonPrimitive("item").getAsString().equals("bookD"));
		assertTrue(test.getDocument(0).getAsJsonPrimitive("price").getAsString().equals("30"));
		delete.dropDatabase();
	}
	
	@Test
	public void testDrop() {
		DB drop = new DB("drop");
		DBCollection test = drop.getCollection("test");
		DBCollection test2 = drop.getCollection("test2");
		assertTrue(drop.DBhasCollections("test"));
		assertTrue(drop.DBhasCollections("test2"));
		File file = new File(test.getCollectionPath());
		File file2 = new File(test2.getCollectionPath());
		
		assertTrue(file.exists());
		test.drop();
		assertFalse(file.exists());
		assertTrue(file2.exists());
	    test2.drop();
	    assertFalse(file2.exists());
		drop.dropDatabase();		
	}
	
}
