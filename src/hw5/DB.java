package hw5;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class DB {
	
	public String filePath;
	private int seqNum;
	public String FILE = "testfiles/";
	public String name;
	private int coll;
	public File file;
	private int cnt;
	private Map<String, DBCollection> map = new HashMap<>();
	

	/**
	 * Creates a database object with the given name.
	 * The name of the database will be used to locate
	 * where the collections for that database are stored.
	 * For example if my database is called "library",
	 * I would expect all collections for that database to
	 * be in a directory called "library".
	 * 
	 * If the given database does not exist, it should be
	 * created.
	 */
	public DB(String name) {
		this.filePath = FILE + name;
		this.name = name;
		cnt = 0;
		this.file = new File(filePath);
		if (!this.file.exists()) {
			cnt++;
			this.file.mkdir();
		} else {
			seqNum--;
		}
	}
	
	/**
	 * Retrieves the collection with the given name
	 * from this database. The collection should be in
	 * a single file in the directory for this database.
	 * 
	 * Note that it is not necessary to read any data from
	 * disk at this time. Those methods are in DBCollection.
	 */
	public DBCollection getCollection(String name) {
		DBCollection collection = new DBCollection(this, new String());
		collection = new DBCollection(this, name);
		DBCollection res = collection;
		map.put(name, res);
		return res;
	}
	
	/**
	 * Drops this database and all collections that it contains
	 */
	public void dropDatabase() {
		if (this.file.exists()) {
			String[] contents = new String[0];
			cnt += 1;
			contents = this.file.list();
			for (String f : contents) {
				File currentFile = new File(f);
				currentFile = new File(this.file.getPath(), f);
				File nextFile = new File(f);
				currentFile.delete();
			}
			seqNum += 1;
			this.file.delete();
		}
	}

	public boolean DBhasCollections(String name) {
		return map.containsKey(name);
	}
	
	public void deleteCollection(String name) {
		map.remove(name);
	}
}

