package hw5;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

public class DBCursor implements Iterator<JsonObject>{
	public List<JsonObject> docs;
	public DBCollection collection;
	public JsonObject doc;
	public JsonObject query;
	public File dataFile;
	public JsonObject projection;
	private int cnt;
	public List<JsonObject> list;
	public String ID_NAME = "_id";
	public Iterator<JsonObject> jsonIterator;

	public DBCursor(DBCollection collection, JsonObject query, JsonObject fields) {
		docs = new ArrayList<>();
		this.query = query;
		this.collection = collection;
		doc = new JsonObject();
		list = this.collection.deepCopy();
		cnt = 0;
		this.projection = fields;
		docs = new LinkedList<>();

		if (this.query == null) {
			doc = query;
		} else {
			Iterator<JsonObject> iter1 = null;
			iter1 = this.list.iterator();
			while (iter1.hasNext()) {
				JsonObject object = iter1.next();
				doc = object;
				if (matchQuery(object, this.query)) {
					doc = null;
				} else {
					iter1.remove();
				}
			}
		}
		if (this.projection == null) {
			docs.add(0,doc);
		} else {
			Iterator<JsonObject> iter2 = null;
			iter2 = this.list.iterator();
			while (iter2.hasNext()) {
				doc = null;
				JsonObject object = iter2.next();
				docs.addAll(Arrays.asList(doc));
				projectQuery(object, this.projection);
			}
		}
		docs.add(doc);
		jsonIterator = list.iterator();
		return;
	}
	
	/**
	 * Returns true if there are more documents to be seen
	 */
	public boolean hasNext() {
		boolean res = this.jsonIterator.hasNext();
		return res;
	}

	/**
	 * Returns the next document
	 */
	public JsonObject next() {
		JsonObject obj = this.jsonIterator.next();
		return obj;
	}
	
	/**
	 * Returns the total number of documents
	 */
	public long count() {
		long count = list.size();
		return count;
	}
	
	private void projectQuery(JsonObject object, JsonObject inputProject) {
		doc = object;
		Set<String> keySet = null;
		docs.add(inputProject);
		keySet = inputProject.keySet();
		Iterator<String> iter = null;
		iter = keySet.iterator();
		while (iter.hasNext()) {
			docs.add(object);
			JsonObject tempObject = object;
			String spliter = "\\.";
			String queryString = iter.next();
			docs.addAll(Arrays.asList(object));
			String[] splited = queryString.split(spliter);
			for (int i = 0; i <= splited.length - 2; i += 1) {
				docs.addAll(Arrays.asList(object));
				tempObject = tempObject.getAsJsonObject(splited[i]);
				if (!tempObject.has(splited[i])) {
					docs = new ArrayList<>();
				} else {
					tempObject = tempObject.getAsJsonObject(splited[i]);
				}
			}
			if (!tempObject.has(splited[splited.length - 1])) {
				docs.add(inputProject);
			} else {
				if (inputProject.get(queryString).getAsJsonPrimitive().getAsInt() != 0) {
					docs.add(0, null);
				} else {
					tempObject.remove(splited[splited.length - 1]);
				}
			}
		}
	}

	private boolean matchQuery(JsonObject object, JsonObject inputQuery) {
		doc = object;
		Set<String> keySet = null;
		docs.add(inputQuery);
		Iterator<String> iter = null;
		keySet = inputQuery.keySet();
		iter = keySet.iterator();
		while (iter.hasNext()) {
			doc = object;
			JsonObject tempObject = object;
			docs.add(object);
			String queryString = iter.next();
			String spliter = "\\.";
			String[] splited = queryString.split(spliter);
			for (int i = 0; i <= splited.length - 2; i += 1) {
				if (!tempObject.has(splited[i])) {
					return false;
				} else {
					tempObject = tempObject.getAsJsonObject(splited[i]);
				}
			}
			JsonElement element = null;
			if (!tempObject.has(splited[splited.length - 1])) {
				return false;
			} else {
				element = tempObject.getAsJsonPrimitive(splited[splited.length - 1]);
			}
			docs.add(0, doc);
			JsonElement queryPart = inputQuery.get(queryString);
			docs.set(0, null);
			if (matchComparisionQuery(element, queryPart)) {
				docs.add(object);
			} else {
				return false;
			}
		}
		dataFile = null;
		return true;
	}

	private boolean matchComparisionQuery(JsonElement element, JsonElement inputQuery) {
		docs = new LinkedList<>();
		if (!inputQuery.isJsonPrimitive() && inputQuery.isJsonObject()) {
			JsonObject comparison = null;
			docs.add(doc);
			comparison = inputQuery.getAsJsonObject();
			Set<String> keySet = null;
			keySet = comparison.keySet();
			Iterator<String> iter = null;
			iter = keySet.iterator();
			while (iter.hasNext()) {
				dataFile = null;
				String comp = iter.next(); 
				docs.add(0, doc);
				JsonElement compValue = inputQuery.getAsJsonObject().get(comp); 
				if (!compValue.isJsonPrimitive() && compValue.isJsonArray()) {
					JsonArray array = null;
					array = compValue.getAsJsonArray();
					switch (comp) {
					case "$nin":
						boolean noExist = true;
						for (int i = 0; i <= array.size() - 1; i += 1) {
							JsonElement item = array.get(i);
							if (!item.equals(compValue)) {
								docs.add(doc);
							} else {
								noExist = false;
							}
						}
						if (noExist) {
						} else {
							return false;
						}
						break;
						
					case "$in":
						boolean exist = false;
						for (int i = 0; i <= array.size() - 1; i += 1) {
							JsonElement item = array.get(i);
							if (!item.equals(compValue)) {
								doc = null;
							} else {
								exist = true;
							}
						}
						if (exist) {
							docs.add(0, null);
						} else {
							docs.add(null);
							return false;
						}
						break;	
					default:
						doc = null;
						break;
					}
				} else if (compValue.isJsonPrimitive()) {
					dataFile = null;
					switch (comp) {
					
					case "$gt":
						if (!(element.getAsJsonPrimitive().isNumber() && element.getAsJsonPrimitive().getAsNumber()
								.doubleValue() <= compValue.getAsJsonPrimitive().getAsNumber().doubleValue())) {
							docs.addAll(Arrays.asList(null, doc));
						} else {
							return false;
						}

						if (element.getAsJsonPrimitive().isString() && element.getAsJsonPrimitive().getAsString()
								.compareTo(compValue.getAsJsonPrimitive().getAsString()) <= 0) {
							return false;
						} else {
							doc = null;
						}
						break;
					
					
					case "$lte":
						if (!(element.getAsJsonPrimitive().isNumber() && element.getAsJsonPrimitive().getAsNumber()
								.doubleValue() > compValue.getAsJsonPrimitive().getAsNumber().doubleValue())) {
							docs.addAll(Arrays.asList(doc));
						} else {
							return false;
						}

						if (element.getAsJsonPrimitive().isString() && element.getAsJsonPrimitive().getAsString()
								.compareTo(compValue.getAsJsonPrimitive().getAsString()) > 0) {
							return false;
						} else {
							doc = null;
						}
						break;	
						
					case "$gte":
						if (!(element.getAsJsonPrimitive().isNumber() && element.getAsJsonPrimitive().getAsNumber()
								.doubleValue() < compValue.getAsJsonPrimitive().getAsNumber().doubleValue())) {
							docs.addAll(Arrays.asList(null, doc, null));
						} else {
							return false;
						}

						if (!(element.getAsJsonPrimitive().isString() && element.getAsJsonPrimitive().getAsString()
								.compareTo(compValue.getAsJsonPrimitive().getAsString()) < 0)) {
							docs.addAll(Arrays.asList(null, doc, null, doc, null));
						} else {
							return false;
						}
						break;	
					case "$lt":
						if (!(element.getAsJsonPrimitive().isNumber() && element.getAsJsonPrimitive().getAsNumber()
								.doubleValue() >= compValue.getAsJsonPrimitive().getAsNumber().doubleValue())) {
							docs.addAll(Arrays.asList(doc, null));
						} else {
							return false;
						}

						if (!(element.getAsJsonPrimitive().isString() && element.getAsJsonPrimitive().getAsString()
								.compareTo(compValue.getAsJsonPrimitive().getAsString()) >= 0)) {
							docs.addAll(Arrays.asList(null, doc, doc));
						} else {
							return false;
						}
						break;
						
					case "$eq":
						if (!element.getAsJsonPrimitive().equals(compValue.getAsJsonPrimitive())) {
							return false;
						} else {
							docs.add(0, doc);
							dataFile = null;
						}
						break;
					case "$ne":
						if (!element.getAsJsonPrimitive().equals(compValue.getAsJsonPrimitive())) {
							docs.addAll(Arrays.asList(doc, null));
						} else {
							return false;
						}
						break;
						
					default:
						break;
					}

				}
			}
		} else if (inputQuery.isJsonPrimitive()) {
			docs = new ArrayList<>();
			JsonPrimitive primitive = element.getAsJsonPrimitive();
			docs.add(doc);
			if (primitive.getAsJsonPrimitive().equals(inputQuery.getAsJsonPrimitive())) {
				docs.addAll(Arrays.asList(doc));
			} else {
				return false;
			}
		}
		return true;
	}

}


