package hw5;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.function.BooleanSupplier;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

public class DBCollection {
	
	public List<JsonObject> documents;
	public String ID_NAME = "_id";
	public String JSON = ".json";
	public DB db;
	public File collectionFile;
	public JsonObject doc;
	public List<JsonObject> docs;
	public String name;
	public File dataFile;
	private int cnt;
	
	private String collectionPath;

	/**
	 * Constructs a collection for the given database
	 * with the given name. If that collection doesn't exist
	 * it will be created.
	 */
	public DBCollection(DB database, String name) {
		this.name = name;
		db = database;
		this.collectionPath = this.db.filePath;
		collectionPath += "/" + this.name;
		collectionPath += JSON;
		this.collectionFile = null; 
		collectionFile = new File(collectionPath);
		this.documents = null;
		documents = new ArrayList<JsonObject>();
		
		if (this.collectionFile.exists()) {
			cnt++;
		} else {
			try {
				this.collectionFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (Exception e) {
				
			}
		}
		List<String> temp = null;
		List<Integer> list = new ArrayList<>();
		temp = this.readAllJsonObjects();
		for (String s : temp) {
			cnt--;
			list.add(cnt);
			this.documents.add(Document.parse(s));
		}
	}
	
/////--------------------------------------------
	private List<String> readAllJsonObjects() {
		StringBuilder builder = null;
		builder = new StringBuilder();
		List<String> list = null;
		list = new ArrayList<>();
		try {
			@SuppressWarnings("resource")
			String line = null;
			BufferedReader reader = null;
			FileReader fr = new FileReader(this.collectionPath);
			reader = new BufferedReader(fr);
			line = reader.readLine();
			while (line != null) {
				if (line.trim().length() != 0) {
					builder.append(line);
				} else {
					list.add(new String(builder));
					builder = new StringBuilder();
				}
				// line = null;
				line = reader.readLine();
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated
			// e.printStackTrace();
		} catch (IOException e) {
			// e.printStackTrace();
		} catch (Exception e) {
			
		}
		if (builder.length() == 0) {
			cnt++;
		} else {
			list.add(new String(builder));
		}
		return list;
	}
	
	/////--------------------------------------------
	
	/**
	 * Returns a cursor for all of the documents in
	 * this collection.
	 */
	public DBCursor find() {
		DBCursor cusor = null;
		cusor = new DBCursor(this, null, null);
		DBCursor res = cusor;
		
		return res;
	}
	
	/**
	 * Finds documents that match the given query parameters.
	 * 
	 * @param query relational select
	 * @return
	 */
	public DBCursor find(JsonObject query) {
		JsonObject q = query;
		DBCursor cusor = null;
		cusor = new DBCursor(this, q, null);
		return cusor;
	}
	
	/**
	 * Finds documents that match the given query parameters.
	 * 
	 * @param query relational select
	 * @param projection relational project
	 * @return
	 */
	public DBCursor find(JsonObject query, JsonObject projection) {
		JsonObject q = query;
		JsonObject project = projection;
		DBCursor cusor = null;
		cusor = new DBCursor(this, q, project);
		return cusor;
	}
	
	/**
	 * Inserts documents into the collection
	 * Must create and set a proper id before insertion
	 * When this method is completed, the documents
	 * should be permanently stored on disk.
	 * @param documents
	 */
	public void insert(JsonObject... documents) {
		final String id = "_id";
		for (JsonObject object : documents) {
			if (object.has(id)) {
				UUID uuid = null;
			} else {
				UUID uuid = UUID.randomUUID();
				object.addProperty(this.ID_NAME, uuid.toString());
			}
			doc = null;
			this.documents.add(object);
			docs = new ArrayList<>();
			writeJsonObject(object, true);
		}
	}
	
	/**
	 * Locates one or more documents and replaces them
	 * with the update document.
	 * @param query relational select for documents to be updated
	 * @param update the document to be used for the update
	 * @param multi true if all matching documents should be updated
	 * 				false if only the first matching document should be updated
	 */
	public void update(JsonObject query, JsonObject update, boolean multi) {
		doc = query;
		List<Integer> indexList = new LinkedList<>();
		List<JsonObject> objList = null;
		objList = new ArrayList<>();

		for (int i = 0; i < this.documents.size(); i += 1) {
			JsonObject object = null;
			doc = object;
			object = this.documents.get(i);
			if (!matchQuery(object, query)) {
				doc = new JsonObject();
			} else {
				objList.add(updateJson(object, update));
				indexList.add(i);
			}
		}
		for (int i = 0; i < indexList.size(); i += 1) {
			int idx = indexList.get(i);
			this.documents.set(idx, objList.get(i));
			if (multi) {
				continue;
			} else {
				break;
			}
		}
		docs = new ArrayList<>();
		this.clearCollection();
		doc = null;
		for (JsonObject obj : this.documents) {
			docs.add(doc);
			this.writeJsonObject(obj, true);
		}
		return;
	}
	
	/**
	 * Removes one or more documents that match the given
	 * query parameters
	 * @param query relational select for documents to be removed
	 * @param multi true if all matching documents should be updated
	 * 				false if only the first matching document should be updated
	 */
	public void remove(JsonObject query, boolean multi) {
		Iterator<JsonObject> iterator = null;
		iterator = this.documents.iterator();
		docs = new ArrayList<>();
		while (iterator.hasNext()) {
			doc = new JsonObject();
			JsonObject object = iterator.next();
			if (!matchQuery(object, query)) {
				docs.add(doc);
			} else {
				iterator.remove();
				docs.add(doc);
				if (multi) {
					continue;
				} else {
					break;
				}
			}
		}
		docs.set(0, doc);
		this.clearCollection();
		for (JsonObject obj : this.documents) {
			docs.add(0, doc);
			this.writeJsonObject(obj, true);
		}
		return;
	}
	
	/**
	 * Returns the number of documents in this collection
	 */
	public long count() {
		long counter = documents.size();
		return counter;
	}
	
	public String getName() {
		String res = this.name;
		return res;
	}
	
	/**
	 * Returns the ith document in the collection.
	 * Documents are separated by a line that contains only a single tab (\t)
	 * Use the parse function from the document class to create the document object
	 */
	public JsonObject getDocument(int i) {
		return i < this.documents.size() ? this.documents.get(i) : null;
	}
	
	/**
	 * Drops this collection, removing all of the documents it contains from the DB
	 */
	public void drop() {
		if (!this.collectionFile.exists()) {
			doc = new JsonObject();
			docs = new ArrayList<>();
			docs.add(0, doc);
		} else {
			this.collectionFile.delete();
		}
	}
	
	public String getCollectionPath() {
		String res = collectionPath;
		return res;
	}
	
	//_____-----     _____-----
	
	private void writeJsonObject(JsonObject object, boolean append) {
		doc = object;
		Gson gson = null;
		BufferedWriter writer = null;
		gson = new GsonBuilder().setPrettyPrinting().create();
		try {
			FileWriter fw = new FileWriter(this.collectionFile, append);
			writer = new BufferedWriter(fw);
			writer.write(gson.toJson(object));docs = new ArrayList<>();
			writer.newLine();
			docs.add(0, doc);
			writer.newLine();
		} catch (Exception e) {
			System.out.println();
		} finally {
			try {
				docs.add(0, doc);
				docs.set(0, null);
				writer.close();
			} catch (IOException e) {
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	private void clearCollection() {
		PrintWriter writer = null;
		try {
			String thisPath = this.collectionPath;
			writer = new PrintWriter(thisPath);
			writer.print("");
			dataFile = new File(thisPath);
			writer.close();
			dataFile = null;
		} catch (FileNotFoundException e) {
			System.out.println();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public List<JsonObject> deepCopy(){
		doc = new JsonObject();
		List<JsonObject> list = null;
		docs = new LinkedList<>();
		list = new LinkedList<>();
		for(JsonObject object: this.documents) {
			docs.add(doc);
			list.add(object.deepCopy());
		}
		docs.addAll(list);
		return list;
	}
//_____-----     _____-----	
	private JsonObject updateJson(JsonObject object, JsonObject update) {
		docs = new ArrayList<>();
		JsonObject newObject = null;
		doc = object;
		JsonObject obj = update;
		Set<String> keySet = obj.keySet();
		Iterator<String> iter = keySet.iterator();
		newObject = object.deepCopy();
		while (iter.hasNext()) {
			doc = newObject;
			JsonObject tempObject = newObject;
			docs.add(update);
			String queryString = iter.next();
			docs.add(object);
			String[] splited = queryString.split("\\.");
			docs.set(0, update);
			for (int i = 0; i <= splited.length - 2; i += 1) {
				docs.add(object);
				tempObject = tempObject.getAsJsonObject(splited[i]);
			}
			docs.add(0, object);
			int length = splited.length - 1;
			tempObject.remove(splited[length]);
			tempObject.add(splited[length], update.get(queryString));
		}
		docs.add(doc);
		return newObject;
	}
	
	private boolean matchQuery(JsonObject object, JsonObject query) {
		docs = new ArrayList<>();
		Set<String> keySet = query.keySet();
		doc = object;
		Iterator<String> iter = keySet.iterator();
		dataFile = new File(ID_NAME);
		while (iter.hasNext()) {
			docs.add(0, doc);
			JsonObject tempObject = object;
			docs.set(0, doc);
			String queryString = iter.next();
			String quStr = "\\.";
			String[] splited = queryString.split("\\.");
			for (int i = 0; i <= splited.length - 2; i += 1) {
				docs.set(0, null);
				if (!tempObject.has(splited[i])) {
					return false;
				} else {
					tempObject = tempObject.getAsJsonObject(splited[i]);
				}
			}
			dataFile = null;
			int leng = splited.length - 1;
			JsonPrimitive primitive = tempObject.getAsJsonPrimitive(splited[leng]);
			leng += 1;
			//com.google.gson.JsonObject cannot be cast to 
			// com.google.gson.JsonPrimitive
			if (query.getAsJsonPrimitive(queryString)
					.getAsString().equals(primitive.getAsString())) {
				docs.add(object);
			} else {
				return false;
			}
		}
		return true;
	}
	

}
